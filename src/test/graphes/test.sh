#!/bin/bash
# ----------------------------------------------------------------------------
# Test script
# ----------------------------------------------------------------------------
TESTNO=$1

./graph < input_${TESTNO}_fixture.txt > output_${TESTNO}_actual.dot
diff output_${TESTNO}_expected.dot output_${TESTNO}_actual.dot && echo "FC: no differences encountered"
