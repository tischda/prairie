:: ----------------------------------------------------------------------------
:: Test script
:: ----------------------------------------------------------------------------
@echo off
setlocal

set TESTNO=%1

graph.exe < input_%TESTNO%_fixture.txt > output_%TESTNO%_actual.dot
fc output_%TESTNO%_expected.dot output_%TESTNO%_actual.dot
endlocal
