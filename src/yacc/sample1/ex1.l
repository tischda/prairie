%{
#include <stdio.h>
%}

%%
stop    printf("Stop command received\n");
start   printf("Start command received\n");
%%

int main(void)
{
    yylex();
    return 0;
}
