%{ /* graph.l */
#include <string.h>
#include "graph.h"
#include "graph_parser.h"
%}

%%
[0-9]+  { yylval.strval = strdup(yytext); return VERTEX; }
[A-Z]*  { yylval.strval = strdup(yytext); return NAME; }

[ \t\n\r]   ;               /* ignorer les espaces */
"$"     return 0;           /* fin du traitement */
.       return yytext[0];   /* retourner le caractere */
%%
