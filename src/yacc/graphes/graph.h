/* graph.h */
#ifndef GRAPH_H
#define GRAPH_H

/* Detect memory leaks (works only in debug mode) */
/* http://msdn.microsoft.com/en-us/library/x98tx3cf.aspx */
#if defined(WIN32)
  #define _CRTDBG_MAP_ALLOC
  #include <stdlib.h>
  #include <crtdbg.h>
#endif

extern int yylex();
extern void yyerror();
extern int yylex_destroy (void);

#endif
