%error-verbose

%{ /* graph.y */
#include <stdio.h>
#include <string.h>
#include "graph.h"

void yyerror(const char* s) {
  printf("Error : %s\n", s);
}
static const char GrphFormat[] = "digraph %s {%s\n}\n";
static const char edgeFormat[] = "\n    %s -> %s;";
%}

/* lists all possible types for values associated with
   parts of the grammar and gives each a field-name */
%union {
    char* strval;
}

%token <strval> NAME
%token <strval> VERTEX

%type <strval> graph
%type <strval> list
%type <strval> edge

%start S

%%
S: graph
    | S graph
    ;

graph: NAME '=' '{' list '}'  {
          printf(GrphFormat, $1, $4);
          free($1);
          free($4);
        }
	;

list: edge
    | list ',' edge            {
          $$ = (char*) malloc(strlen($1) + strlen($3) + 1);
          sprintf($$, "%s%s", $1, $3);
          free($1);
          free($3);
       }
    ;

edge: '(' VERTEX ',' VERTEX ')' {
          $$ = (char*) malloc(strlen($2) + strlen($4) + 1 + sizeof(edgeFormat));
          sprintf($$, edgeFormat, $2, $4);
          free($2);
          free($4);
      }
    ;

%%
int main(void) {

/* Memory leak detection */
/* http://msdn.microsoft.com/en-us/library/x98tx3cf.aspx */
#if defined(WIN32)
    /* will cause an automatic call to _CrtDumpMemoryLeaks at each exit point */
    _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

    // Send all reports to STDOUT
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDERR );
    _CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_ERROR, _CRTDBG_FILE_STDERR );
    _CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDERR );
#endif

    yyparse();

    /* yylex_destroy should be called to free resources used by the scanner */
    yylex_destroy();
}
