/* input " x X abc  aBc AAV 1 343E3" */

/* priorite a la regle qui reconnait la plus longue sequence */

/* cas 3: ici la regle 2 est comprise dans la regle 1 => warning ! */

%%
[a-zA-Z]+   printf("(%s)", yytext);
[a-z]+      printf("[%s]", yytext);
.|\n        ;


%%
int main(void)
{
    yylex();
    return 0;
}
