Flex + Bison example
====================

http://pltplp.net/lex-yacc/example.html.en

Example usage:

~~~
$ calc
 1+2*3
 Result : 7.000000
 2.5*(3.2-4.1^2)
 Result : -34.025000
~~~
