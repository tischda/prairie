/* attention: un espace devant veut dire "code C" ne pas interpreter ! */

 int nb_lignes = 0;

%%

abc { printf("[%s]\n", yytext); yyless(1); printf("(%s)\n", yytext); }
cde { printf("[%s]\n", yytext); yyless(2); printf("(%s)\n", yytext); }
.   { printf("<%c>\n", yytext[0]); }


 /* ECHO affiche la chaine detectee */

\n  { nb_lignes++; ECHO; }

%%

int main() {
    yylex();
    printf("%d lignes\n", nb_lignes);
    return nb_lignes;
}
