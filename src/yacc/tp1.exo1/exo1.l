/* regle implicite: sortie standard */

/* le programme supprime espaces tabs en debut de ligne */


/* quand on enleve l'espace, ca ne marche plus:
   lex demande un espace devant les expr. regul.

   pour traiter les tabs uniquement:


\t+     printf("........");

   et debut de ligne seulement:

^\t+    printf("........");

*/


%%
^[ \t]+ ;

%%

int main(void)
{
    yylex();
    return 0;
}
