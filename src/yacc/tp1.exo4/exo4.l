/* exercice D.16 */

%{
int count = 0;
%}

%%
e      { printf("."); count++; }
.|\n   ;

%%

int main() {
    yylex();
    printf("\n%d fois la lettre e\n", count);
    return count;
}
