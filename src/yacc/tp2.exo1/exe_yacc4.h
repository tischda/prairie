/* exe_yacc4.h */
#ifndef EXE_YACC4_H
#define EXE_YACC4_H
#define NBSYMB 256
#define TAILLEBUF 4096

struct sTableSymb
{
    char * Nom;
    double Valeur;
};

/* D.18.5 */
struct sArbreSyntaxe
{
    char Chaine[TAILLEBUF];
    double Valeur;
};

struct sTableSymb * RechercheSymbole(char * s);

extern int yylex();
extern void yyerror();

#endif
