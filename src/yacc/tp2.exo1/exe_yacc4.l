
%{
/* exe_yacc4.l */

#include <string.h>
#include "exe_yacc4.h"
#include "exe_yacc4_parser.h"

struct sTableSymb TableSymb[NBSYMB];
%}

%%
(([0-9]+)|([0-9]*\.[0-9]+)|([0-9]+\.[0-9]*))([eE][+-]?[0-9]+)?  {  /* nombre */
        yylval.Valeur = atof(yytext); return NOMBRE; }

[ \t]   ;   /* ignorer les espaces */

[a-zA-Z][a-zA-Z0-9]* { /* nom de variable */
        yylval.PtrSymb = RechercheSymbole(yytext); return NOM;
    }

"$"     return 0;   /* fin du traitement */

"#"     {   /* affichage de la table des symboles */
            struct sTableSymb * sp;
            printf("Table des symboles : \n");
            for (sp = TableSymb; sp->Nom; sp++)
            {
                printf("%s = %g\n", sp->Nom, sp->Valeur);
            }
        }

"+="    return PLUSEQUAL;

"-="    return MINUSEQUAL;

"**"    return '^';

"\n"    |
.       return yytext[0];   /* retourner le caractère */
%%

struct sTableSymb * RechercheSymbole(char * s) {
    struct sTableSymb * sp;

    for (sp = TableSymb; sp < &TableSymb[NBSYMB]; sp++) {
        if (sp->Nom && !strcmp(sp->Nom,s)) {
            return sp;
        }
        if (!sp->Nom) {
            sp->Nom = strdup(s);
            return sp;
        }
    }

    yyerror("Dépassement de capacité pour la table des symboles.");
    exit(1);
}
