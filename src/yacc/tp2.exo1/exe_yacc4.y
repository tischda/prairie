%error-verbose

%{
/* exe_yacc4.y */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "exe_yacc4.h"

void yyerror(const char* s) {
  printf("Erreur : %s\n", s);
}

/* D.18.2 : stocke le dernier resultat */
struct sArbreSyntaxe last;

/* D.18.5 : ajoute la valeur a la chaine */
void ajouterFeuille(char * chaine, char type, double valeur) {
    sprintf(chaine, "[.%c\\\\(%g) ]", type, valeur);
}

void ajouterNoeud(char * chaine, char type, double valeur, char * fils1, char * fils2) {
    sprintf(chaine, "[.%c\\\\(%g) %s %s ]", type, valeur, fils1, fils2);
}

%}

%union {
    double Valeur;
    struct sArbreSyntaxe Arbre;
    struct sTableSymb * PtrSymb;
}

%token <PtrSymb> NOM
%token <Valeur> NOMBRE

%token PLUSEQUAL
%token MINUSEQUAL

/* pas besoin de definir token pour operations unitaires, car on utilise
   la valeur du code ASCII. Sinon, il faudrait definir un nouveau token */

/* prioritee dans l'ordre du moins prioritaire au plus prioritaire */
%left	'+'	'-'
%left	'*'	'/'
%right  '^'
%nonassoc MOINSUNITAIRE

%type <Arbre> expr

%%
liste_inst: inst '\n'
	| liste_inst inst '\n'
	;

inst: NOM '=' expr        { $1->Valeur  = $3.Valeur; last.Valeur = $1->Valeur; }
    | NOM PLUSEQUAL expr  { $1->Valeur += $3.Valeur; last.Valeur = $1->Valeur; }
    | NOM MINUSEQUAL expr { $1->Valeur -= $3.Valeur; last.Valeur = $1->Valeur; }
	| expr             	  { printf("> %g\n", $1.Valeur); last = $1; }
    |  /* ce pipe permet d'entrer des lignes vides */
	;

expr: expr '+' expr       { $$.Valeur = $1.Valeur + $3.Valeur;     ajouterNoeud($$.Chaine, '+', $$.Valeur, $1.Chaine, $3.Chaine); }
    | expr '-' expr       { $$.Valeur = $1.Valeur - $3.Valeur;     ajouterNoeud($$.Chaine, '-', $$.Valeur, $1.Chaine, $3.Chaine); }
    | expr '*' expr       { $$.Valeur = $1.Valeur * $3.Valeur;     ajouterNoeud($$.Chaine, '*', $$.Valeur, $1.Chaine, $3.Chaine); }
    | expr '^' expr       { $$.Valeur = pow($1.Valeur, $3.Valeur); ajouterNoeud($$.Chaine, '^', $$.Valeur, $1.Chaine, $3.Chaine); }
    | expr '/' expr
    {
        if ( $3.Valeur == 00 )
            yyerror("divison par zéro");
        else {
            $$.Valeur = $1.Valeur / $3.Valeur;
            ajouterNoeud($$.Chaine, '/', $$.Valeur, $1.Chaine, $3.Chaine);
        }
    }
    | '-' expr  %prec MOINSUNITAIRE { $$.Valeur = -$2.Valeur; }
    | '(' expr ')'        { $$.Valeur = $2.Valeur;  strcpy($$.Chaine, $2.Chaine); }
    | NOMBRE              { $$.Valeur = $1;         ajouterFeuille($$.Chaine, 'n', $1); }
    | NOM                 { $$.Valeur = $1->Valeur; ajouterFeuille($$.Chaine, 'v', $1->Valeur); }
    | '!'                 { $$ = last; }
    | '%'                 { printf("\\Tree %s\n", last.Chaine); }
    ;

%%

int main(void) {
  yyparse();
}

