Exercice D.18 - Analyse Syntaxique
==================================

Example usage:

~~~
$ ./target/tp2.exo1/exe_yacc4
 1+2*3
 Result : 7.000000
 2.5*(3.2-4.1^2)
 Result : -34.025000
~~~


Usage for D.18.5:

~~~
$ ./target/tp2.exo1/exe_yacc4 < src/tp2.exo1/input.txt
Table des symboles :
a = 2
b = 45
c = 14
> 1262
\Tree [.-\\(1262) [.n\\(2) ] [.*\\(-1260) [.*\\(630) [.n\\(45) ] [.n\\(14) ] ] [.-\\(-2) [.v\\(12) ] [.n\\(14) ] ] ] ]
> 0
~~~


Compile LaTeX
-------------
~~~
pdflatex tree.tex
~~~
