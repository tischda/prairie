cmake_minimum_required(VERSION 2.8)

project(exo5)
set ( BINNAME exo5 )

INCLUDE(../../../include/win32.cmake)

FIND_PACKAGE(FLEX REQUIRED)

FLEX_TARGET(MyScanner ${CMAKE_CURRENT_SOURCE_DIR}/exo5.l ${CMAKE_CURRENT_BINARY_DIR}/exo5_lex.c)

include_directories(${CMAKE_CURRENT_BINARY_DIR})
add_executable( ${BINNAME} ${FLEX_MyScanner_OUTPUTS} )

target_link_libraries( ${BINNAME} ${FLEX_LIBRARIES} )
