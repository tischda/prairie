/* exercice D.17 */

SIGN    [+|-]
PENT    [0-9]+
PDEC    \.[0-9]+
PSCI    e[+|-]?[0-9]+

/* on veut  ignorer les nombres dans le bloc:

    \begin{equation}
    432e-342
    \end{equation}
*/

/* definition d'un contexte exclusif */

%x EQUATION

%%

\\begin\{equation\}             { BEGIN EQUATION; ECHO; }
<EQUATION>\\end\{equation\}     { BEGIN INITIAL; ECHO; }

<EQUATION>.|\n    { ECHO; }


 /* le point d'interrogation: element facultatif (apparait 0 ou 1 fois) */

{SIGN}?({PENT}|{PDEC}|{PENT}{PDEC}){PSCI}?  printf("$%s$", yytext);

%%
int main(void)
{
    yylex();
    return 0;
}
