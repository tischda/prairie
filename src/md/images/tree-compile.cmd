@echo off
pdflatex -shell-escape tree.tex
pdfcrop tree.pdf

del /q tree.log
del /q tree.pdf
del /a tree.aux

