---
grade: |
    Département Informatique

    5\ieme{} année

    2013 - 2014

document:
    type: Rapport Compilateurs
    title: Prairie - Lex, Yacc, Bison
    short: Compilateurs
    abstract: |
        Example on how to use Lex and Yacc to process a simple graph DSL.

    keywords: lex, flex, yacc, bison, compiler, grammar, graph, dsl, lexical
    frenchabstract: |

        Note de synthèse sur les compilateurs.

    frenchkeywords: |
        lex, flex, yacc, bison, compilateur, grammaire,
        analyse lexicale, analyse syntaxique, dsl


supervisors:
    category: Encadrant
    list:
        - name: Gilles VENTURINI
          mail: gilles.venturini@univ-tours.fr
    details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
    list:
        - name: Daniel TISCHER
          mail: daniel.tischer@etu.univ-tours.fr
    details: DI5 2013 - 2014

preamble:
    include.tex
---

Introduction
============

Objectif et contexte
--------------------

Ce rapport est une synthèse des travaux relatifs à la compétence "Compilateurs"
en quatrième année (DI4) du cursus Polytech. L'objectif est de prendre en main
les outils `Lex` et `Yacc` et de démontrer leur principe d'utilisation sur un
exemple simple.

Pour compiler la sortie de `Yacc`, j'ai choisi l'outil
[CMake](http://www.cmake.org/) de la société Kitware (par ailleurs spécialiste
dans le domaine de la visualisation de données et de l'imagerie médicale). Ceci
me permet de développer à la fois pour Windows et pour MacOS.


Problème à résoudre
-------------------

Dans les matières _Programmation Mathématique_ (DI3) et de _Théorie des Graphes_
(DI4), j'ai été confronté de façon récurrente, pour la prise de notes comme pour
les exercices, au besoin de visualiser un graphe :

\begin{figure}[!ht]
  \centering
  \includegraphics[width=15cm]{images/et-2014.png}
  \caption{Examen Terminal de Programmation Mathématique (2014)}
\end{figure}

Il existe pour cela des outils très performants comme
[Gephi](https://gephi.org/) ou [GraphStream](http://graphstream-project.org/),
mais par souci de simplicité j'ai choisi [GraphViz](http://graphviz.org/), et
plus particulièrement le format DOT, dont voici un exemple :

~~~
digraph mon_graphe {
     a -> b -> c;
     b -> d;
}
~~~

\begin{wrapfigure}{r}{0.3\textwidth}
  \vspace{-28pt}
  \begin{center}
      \includegraphics[width=4cm]{images/dotgraph.png}
  \end{center}
\end{wrapfigure}

Il s'agit donc de transformer (compiler) le format source de la figure 1.1 vers
le format cible (DOT) afin de pouvoir générer ensuite automatiquement la
représentation du graphe avec la commande :

`dot -Tpng -Grankdir=LR -Nshape=circle dotgraph.dot -o dotgraph.png`

&nbsp;

Principes et outils de compilation
----------------------------------

### Principe

Un compilateur est un programme qui lit un code dans un format source et le
traduit en un code équivalent rédigé dans un autre langage. Ce processus
fonctionne comme une séquence de phases :

\footnotesize

\#  Phase                   Opération
--  ---------------------   ----------------------------------
1.  __Analyse lexicale__    Extraction des séquences de caractères significatives (&rarr; lexèmes)
2.  __Analyse syntaxique__  Structure grammaticale du flot d'unités (&rarr; arbre syntaxique)
3.  __Analyse sémantique__  Contrôle de type et coercition (`int` &rarr; `float`)
4.  __Code intermédiaire__  Représentation intermédiaire (facile à produire et à traduire)
5.  __Optimisation__        Amélioration du code cible (performance, taille)
6.  __Production de code__  Réécriture de la représentation intermédiaire dans le langage cible

\normalsize

Dans ce projet, je ne considère que les deux premières phases qui suffisent à
nos besoins. En effet, la transformation d'un fichier de données décrivant un
graphe ne nécessite pas d'analyse sémantique ou d'optimisation du code.

### Lex et Yacc

Lex est un outil de génération d'analyseurs lexicaux en langage C. Rappelons que
le but de l'analyse lexicale est d'extraire du flux d'entrée les unités
lexicales (_tokens_). Celles-ci viennent en entrée de l'analyseur syntaxique
dans la phase 2.

![Modèle de compilation](images/compilateur.png)

`Yacc` (Yet Another Compiler Compiler) est un outil qui permet de générer, à
partir d'une spécification qui décrit la grammaire du langage, un analyseur
syntaxique en langage C.

`Yacc` est utilisé en association avec `Lex` pour transformer le texte source en
code cible. En effet, le parser généré par `Yacc` fait appel à la fonction
`yylex()` qui provient de `Lex`.

Compilation avec Lex et Yacc
============================

Grammaires
----------

Une grammaire non contextuelle est une grammaire formelle dans laquelle chaque
règle de production est de la forme X &rarr; w où X est un symbole non terminal
et w une chaîne composée de terminaux ou de non-terminaux. Un non terminal X
peut être remplacé par w, sans tenir compte du contexte où il apparaît.

Les règles de dérivation s'expriment ainsi au format de Backus-Naur (BNF) :

`<symbole> ::=` \_\_`expression`\_\_

où `<symbole>` est un non-terminal, et \_\_`expression`\_\_ se compose d'une ou
plusieurs séquences de symboles separées par une barre verticale `|` indiquant
un choix. L'ensemble est une substitution possible pour le symbole de gauche (le
`::=` signifie "est défini par"). Les symboles qui n'apparaissent jamais à
gauche sont des terminaux. En revanche, les symboles à gauche sont des
non-terminaux écrits entre chevrons `<>`.

Notre objectif est la transformation d'une grammaire source en grammaire cible :

&nbsp;

\ttfamily\footnotesize
\begin{tabular}{ p{0.5\textwidth} p{0.5\textwidth} }

<graphe> ::= <nom> "=" "{" <liste> "}"

<liste> ::= <arc> | <liste> "," <arc>

<arc> ::= "(" <sommet> "," <sommet> ")"

&

<graphe> ::= "digraph" <nom> "\{" <liste> "\}"

<liste> ::= <arc> | <liste> <arc>

<arc> ::= <sommet> "->" <sommet>

\end{tabular}
\normalfont\normalsize

Arbre d'analyse
---------------

Une grammaire dérive des chaînes en commençant par l'axiome et en remplaçant de
façon répétée un non-terminal par le résultat d'une des règles de production qui
le définissent. Un arbre d'analyse montre de façon imagée comment l'axiome d'une
grammaire dérive une chaîne du langage :

![Arbre d'analyse du modèle de graphe](images/tree-crop.pdf)

Analyse lexicale
----------------

L'extraction des unités lexicales (_token_) se fait avec l'outil `Lex` par un
fichier de description formé de trois parties, selon le schéma suivant :

~~~
déclarations
%%
règles de traduction
%%
routines auxiliaires
~~~

Voici le fichier qui traite notre représentation de graphe :

~~~
%{ /* graph.l */
#include <string.h>
#include "graph.h"
#include "graph_parser.h"
%}

%%
[0-9]+  { yylval.strval = strdup(yytext); return VERTEX; }
[A-Z]*  { yylval.strval = strdup(yytext); return NAME; }

[ \t\n\r]   ;               /* ignorer les espaces */
"$"     return 0;           /* fin du traitement */
.       return yytext[0];   /* retourner le caractere */
%%
~~~

Chaque règle débute par une expression régulière. Par exemple, le `[0-9]+` à
la ligne 8 correspond à un ou plusieurs chiffres compris entre 0 et 9 qui
identifient un sommet (VERTEX en anglais) du graphe.

Le code qui suit entre accolades est exécuté lorsque l'expression est reconnue
dans le flux d'entrée. Ici on copie la valeur reconnue `yytext` dans la variable
globale `yyval` qui est partagée entre l'analyseur lexical et l'analyseur
syntaxique. La valeur de retour `VERTEX` est l'identifiant correspondant à la
valeur d'attribut de l'unité lexicale ainsi transmise.

Les règles suivantes (ligne 11) permettent d'ignorer les sauts de lignes et les
espaces, et de transmettre les symboles '`=(,){}`' par leur code ASCII, ce qui
nous évite de les définir en tant que _token_ dans `Yacc`.

Analyse syntaxique
------------------

L'analyse syntaxique se fait avec `Yacc`, un générateur de parser LALR
(lookahead-LR)[^1]. Développé dans les années 1970 par AT&T, `Yacc` a été
utilisé pour implémenter de nombreux compilateurs réels.

Le format du fichier de description est identique à celui de `Lex`.

[^1]: Dans "LR", le "L" décrit le parcours  de l'entrée de gauche à droite (en
anglais _left-to-right_), "R" représente le fait que l'on construit à rebours
une dérivation droite (en anglais "rightmost").

### Déclarations

Voici la première partie du code source correspondant à notre transformation de
graphe :

~~~
%{ /* graph.y */
#include <stdio.h>
#include <string.h>
#include "graph.h"

void yyerror(const char* s) {
  printf("Error : %s\n", s);
}
static const char GrphFormat[] = "digraph %s {%s\n}\n";
static const char EdgeFormat[] = "\n    %s -> %s;";
%}

%union {
    char* strval;
}

%token <strval> NAME
%token <strval> VERTEX

%type <strval> graph
%type <strval> list
%type <strval> edge

%start S
~~~

Dans la première section de la partie déclarations, on place les déclarations C
ordinaires, délimitées par `%{` et `%}`. Tout ce qui se trouve entre ces
accolades est copié directement dans le fichier `graph_parser.c`. Dans ce bloc,
je définis la fonction `yyerror()`, qui est appelée en cas de flux d'entrée
invalide, et les constantes de chaînes pour le format de sortie (DOT).

Dans la section suivante on trouve une série de définitions régulières. Le mot
clé `%union` définit `yyval` comme une union classique de type C de tous les
types possibles associés à une partie de la grammaire, en l'occurence ici une
chaîne de caractère (`char*`)[^2] puisqu'on ne convertit pas les sommets du
graphe en valeurs numériques.

[^2]: Ici, avec un seul type, nous n'aurions pas eu besoin de définir une union,
mais je l'ai gardée pour illustrer le principe.

Enfin, on déclare les différentes unités lexicales (_tokens_) transmises par
`Lex`, les types des non-terminaux (`graph`, `list`, `edge`) et l'axiome `S`.

### Règles de traduction

Dans la partie de spécification `Yacc` qui suit le premier couple `%%`, on place
les règles de traduction qui permettent d'interpréter le format en entrée pour
générer un nouveau fichier en sortie. Chaque règle est composée d'une production
de la grammaire et de l'action sémantique associée.

Ces règles se dérivent de l'arbre syntaxique présenté en figure 2.1. Ainsi, un
graphe se compose d'un nom (`NAME`), d'un symbole égale (`=`), d'une accolade
ouvrante, d'une liste d'arcs et enfin d'une accolade fermante.

~~~
%%
S: graph
    | S graph
    ;

graph: NAME '=' '{' list '}'  {
          printf(GrphFormat, $1, $4);
          free($1);
          free($4);
        }
    ;

list: edge
    | list ',' edge            {
          $$ = (char*) malloc(strlen($1) + strlen($3) + 1);
          sprintf($$, "%s%s", $1, $3);
          free($1);
          free($3);
       }
    ;

edge: '(' VERTEX ',' VERTEX ')' {
          $$ = (char*) malloc(strlen($2) + strlen($4) + 1 + sizeof(edgeFormat));
          sprintf($$, edgeFormat, $2, $4);
          free($2);
          free($4);
      }
    ;
~~~

Une action sémantique `Yacc` est une séquence d'instructions C. Lorsqu'un graphe
est reconnu, on l'affiche sur la sortie standard avec la fonction `printf`. Le
paramètre `$1` représente le premier symbole grammatical (`NAME`) et `$4` le
quatrième, donc ici la liste des arcs. Cette liste est compilée à partir de la
règle suivante qui la définit comme une séquence d'arcs (`edge`). Enfin, avec
`free()` on libère la mémoire allouée aux chaînes et lexèmes (cf. `malloc` à la
ligne 15 et `strdup()` dans les lignes 8 et 9 du code `Lex`).

Remarquons que la première règle qui dérive de l'axiome `S` permet de réaliser
plusieurs tests consécutifs en ligne de commande sans devoir redémarrer le
parser.

### Routines auxiliaires

La dernière partie contient la fonction `main()` qui rend le parser `Yacc`
exécutable. L'appel à la fonction `yyparse()` exécute l'analyseur syntaxique
sur le flux d'entrée standard.

~~~
%%
int main(void) {
    yyparse();
    yylex_destroy();
}
~~~

Yacc fait appel à `Lex` pour l'analyse lexicale. Dans cette configuration, `Lex`
ne libère pas sa mémoire, il faut donc également penser à le faire
explicitement en appelant `yylex_destroy()`.

Résultat du traitement
----------------------

La compilation du code C du parser généré par `Yacc` (`graph_parser.c`)
produit un programme exécutable. En lui fournissant en entrée notre fichier
exemple, nous obtenons le résultat suivant :

Entrée :

~~~
V = {(3,2),(4,2),(6,2),(8,2),(1,3),(5,3),(6,3),(7,3),(1,4),(1,5),(4,5),(6,5),(7,6),
(4,7),(4,8),(5,8),(7,8)}
~~~

Sortie :

~~~
digraph V {
    3 -> 2;
    4 -> 2;
    6 -> 2;
    8 -> 2;
    1 -> 3;
    5 -> 3;
    6 -> 3;
    7 -> 3;
    1 -> 4;
    1 -> 5;
    4 -> 5;
    6 -> 5;
    7 -> 6;
    4 -> 7;
    4 -> 8;
    5 -> 8;
    7 -> 8;
}
~~~

Le fichier de sortie (`.dot`) est ensuite donné en entrée à Graphviz pour
produire l'image suivante :

\begin{figure}[!h]
  \centering
  \includegraphics[width=12cm]{images/input_003_actual.png}
  \caption{Réprésentation graphique à partir du fichier de sortie}
\end{figure}

Réalisation technique
=====================

De la théorie à la pratique
---------------------------

Dans le chapitre précédent, nous avons vu une façon simple de réaliser un parser
avec `Lex` et `Yacc`. Cependant, pour passer de la théorie à la pratique, il
faut configurer un certain nombre de paramètres avant de pouvoir démarrer un
nouveau projet.

Tout d'abord, `Lex` et `Berkeley Yacc` ne sont pas libres[^3], je me suis donc
reporté sur `Flex` et `GNU Bison` pour constituer le cheptel de ma
prairie.

J'ai ensuite étudié les différences de configuration pour MacOS et Windows,
chaque système apportant son lot d'incompatibilités. Une fois ces problèmes
réglés, le code pouvait fonctionner sur les deux systèmes.

[^3]: What is the difference between Flex/Lex and Yacc/Bison? [[http://bit.ly/1nZOrEG](http://stackoverflow.com/questions/623503/what-is-the-difference-between-flex-lex-and-yacc-bison)]

Installation de Flex et Bison
-----------------------------

### Apple MacOS

Sous MacOS (Mavericks), on installe d'abord les utilitaires de ligne de commande
(non inclus dans XCode). Ensuite, il faut réinstaller `Flex` et `Bison` avec
[MacPorts](http://www.macports.org/) pour que les librairies nécessaires à
l'édition de lien soient trouvées par CMake:

~~~
sudo port install flex bison cmake
~~~

Pour la même raison, il vaut mieux installer CMake avec MacPorts plutôt que par
la distribution officielle.

### Microsoft Windows

Sous Windows on installe [Visual Studio 2013](http://www.visualstudio.com/en-
us/downloads), [CMake 3.0](http://www.cmake.org/cmake/resources/software.html)
et [Cygwin](https://www.cygwin.com/). On configure ensuite Cygwin pour installer
`Flex` et `Bison`. Il faut faire attention à l'ordre des entrées dans le `PATH`,
car si l'on installe aussi msysGit, on sera peut-être surpris de trouver
d'autres versions de `Flex` et `Bison` dans `c:\Program Files (x86)\Git\bin`.

Ensuite, CMake trouve bien les `Flex` dans Cygwin, mais pas sa librairie
`libfl.a`.

~~~
-- Found BISON: u:/bin/cygwin/bin/bison.exe (found version "2.7.12-4996")
-- Found FLEX: u:/bin/cygwin/bin/flex.exe (found version "2.5.35")
CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
Please set them or make sure they are set and tested correctly in the CMake files:
FL_LIBRARY (ADVANCED)
    linked by target "graph" in directory U:/src/polytech/prairie/src/yacc/graphes
~~~

Le problème semble connu, mais Les solutions proposées[^4] ne fonctionnent pas.
Comme solution alternative, j'ajoute à la ligne 6 du fichier de configuration
CMake (`CMakeLists.txt`) une inclusion spécifique à Windows qui va me permettre
de définir quelques variables et options supplémentaires :

[^4]: Getting CMake to find flex on Windows [[http://bit.ly/1nZOyAc](http://stackoverflow.com/questions/16697549/getting-cmake-to-find-flex-on-windows)]

~~~
cmake_minimum_required(VERSION 2.8)

project(graph)
set (BINNAME graph)

INCLUDE(../../../include/win32.cmake)

FIND_PACKAGE(BISON REQUIRED)
FIND_PACKAGE(FLEX REQUIRED)
...
target_link_libraries( ${BINNAME} ${FLEX_LIBRARIES} )
~~~

Le contenu du fichier `win32.cmake` est le suivant (cf. ligne 10 pour la
librairie de `Flex`) :

~~~
if (WIN32)
    set ( C_FLAGS_WIN64 "/nologo /D_WINDOWS /D_CRT_SECURE_NO_WARNINGS /W3 /GS /D_MBCS /D_REENTRANT /wd4996 /wd4018" )
    set ( CMAKE_C_FLAGS_DEBUG   "${C_FLAGS_WIN64} /D_DEBUG /Od /Z7 /RTC1 /MTd" )
    set ( CMAKE_C_FLAGS_RELEASE "${C_FLAGS_WIN64} /DNDEBUG /O2 /Ob2 /MT" )

    # provide unistd.h from GnuWin32 project
    include_directories( "${CMAKE_CURRENT_SOURCE_DIR}/../../../include/" )

    # libraries are copied from: ...\cygwin\lib\
    set ( FL_LIBRARY "${CMAKE_CURRENT_SOURCE_DIR}/../../../lib/libfl.a" )
endif (WIN32)
~~~

La dernière étape est de créer un script de compilation (`compile.cmd`) qui
prépare l'environnement de Visual Studio (ligne 2) en mode `x86`, en conformité
avec la version 32 bits de Cygwin.

~~~
...
call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x86

cmake -G "NMake Makefiles" ..\..\src\yacc\%OUTPUT% -DCMAKE_BUILD_TYPE=Debug
nmake /nologo
~~~

La ligne 4 crée le Makefile avec CMake, la ligne 5 exécute le `nmake` (make de
Windows) qui génère l'exécutable `graph.exe`, notre parser de graphes :

~~~
graph.exe < input.txt > output.dot
~~~

Grâce à CMake, cette configuration est transposable directement à d'autres
environnements. En particulier, je n'ai plus besoin de modifier ni les sources,
ni les Makefiles du projet. C'est un gain de temps appréciable.

Tests unitaires
---------------

Dans l'optique d'une approche pilotée par les tests, j'ai mis en place des tests
unitaires. Pour cela CMake propose l'outil `ctest` que l'on configure de la
façon suivante dans le fichier de configuration CMake :

`CMakeLists.txt`

~~~
...
enable_testing()
if (WIN32)
    set (EXT cmd)
else ()
    set (EXT sh)
endif ()

add_test(test1 test.${EXT} 001)
add_test(test2 test.${EXT} 002)
add_test(test3 test.${EXT} 003)
set_tests_properties( test1 test2 test3 PROPERTIES PASS_REGULAR_EXPRESSION "FC: no differences encountered" )
~~~

`test.{sh|cmd}`

~~~
#!/bin/bash
TESTNO=$1
./graph < input_${TESTNO}_fixture.txt > output_${TESTNO}_actual.dot
diff output_${TESTNO}_expected.dot output_${TESTNO}_actual.dot && echo "FC: no differences encountered"

# Windows: fc output_%TESTNO%_expected.dot output_%TESTNO%_actual.dot
~~~

`input_001_fixture.txt`

~~~
G = {(1,2)}
~~~

`input_003_fixture.txt`

~~~
V = {(3,2),(4,2),(6,2),(8,2),(1,3),(5,3),(6,3),(7,3),(1,4),(1,5),(4,5),(6,5),(7,6),
(4,7),(4,8),(5,8),(7,8)}
~~~

Exécution :

~~~
Running tests...
Test project U:/src/polytech/prairie/target/graphes
    Start 1: test001
1/3 Test #1: test001 ..........................   Passed    0.05 sec
    Start 2: test002
2/3 Test #2: test002 ..........................   Passed    0.04 sec
    Start 3: test003
3/3 Test #3: test003 ..........................   Passed    0.04 sec

100% tests passed, 0 tests failed out of 3

Total Test time (real) =   0.14 sec
~~~


Retour sur expérience
=====================

Difficultés rencontrées
-----------------------

### Installation

Il est assez étonnant de voir qu'il reste compliqué d'installer un utilitaire
comme `Yacc` sur d'autres systèmes, alors qu'il est présent sur Unix depuis les
années 1970. Même après 40 années, nous sommes confrontés à des problèmes de
licences et de compatibilité.

Alors que tous les ouvrages indiquent que l'on compile un programme `Lex` avec
l'option `-lfl`, sous MacOS il en est autrement[^5] :

~~~
flex -v example1.l
cc lex.yy.c -o example1 -ll -v
~~~

Heureusement, l'utilisation de CMake m'a permis de contourner ces obstacles,
même si sous Windows il a fallu faire le grand écart entre le monde UNIX
(librairies Cygwin) et le compilateur de Microsoft. Une alternative eût été
d'installer `gcc` sous Cygwin, mais cela eût amené d'autres complications[^6].

### Développement

Lorsque le parser ne produit pas le résultat escompté, il est parfois difficile
d'en trouver les raisons. Pour cela on peut définir la fonction `yyerror()` et
spécifier l'option `%error-verbose` en tête du fichier `Yacc`.

Ensuite, les tests unitaires sont une aide précieuse si l'on prend la peine de
les mettre en place dès le départ. En effet, ma première version ne gérait pas
les listes, mais comme j'avais validé les arcs, je pouvais me consacrer au
traitement des listes en toute confiance quant au traitement de ceux dont elles
dépendaient.

[^5]: Using [Flex] on OS X [[http://bit.ly/1nZOEaU](http://linux-digest.blogspot.fr/2013/01/using-flex-on-os-x.html)]
[^6]: Executable file generated using gcc under cygwin [[http://bit.ly/UFaGWv](http://stackoverflow.com/questions/4143629/executable-file-generated-using-gcc-under-cygwin)]

### Phase opérationnelle

La grammaire étant relativement simple à implémenter, le parser a été rapidement
fonctionnel. Je me suis ensuite demandé s'il serait possible d'en faire un
service web pour faire profiter les étudiants de cette nouvelle fonctionnalité
très pratique.

Il faudrait alors faire d'autres tests pour garantir la robustesse du parser.
Sous UNIX, j'aurais pu utiliser [Valgrind](http://valgrind.org/), mais cet outil
n'est pas disponible sous Windows et seulement de façon limitée sous MacOS.

Je me suis donc contenté de l'outillage de bord. Les extraits de code suivants
montrent les changements à faire dans le code source et une exécution manuelle
du programme compilé en mode "debug".

\lstset{frame=trbl,basicstyle={\ttfamily\scriptsize}}

`graph.h`

~~~
/* Detect memory leaks (works only in debug mode) */
/* http://msdn.microsoft.com/en-us/library/x98tx3cf.aspx */
#if defined(WIN32)
  #define _CRTDBG_MAP_ALLOC
  #include <stdlib.h>
  #include <crtdbg.h>
#endif
~~~

`graph.y`

~~~
%%
int main(void) {

/* Memory leak detection */
/* http://msdn.microsoft.com/en-us/library/x98tx3cf.aspx */
#if defined(WIN32)
    /* will cause an automatic call to _CrtDumpMemoryLeaks at each exit point */
    _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

    // Send all reports to STDOUT
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDERR );
    _CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_ERROR, _CRTDBG_FILE_STDERR );
    _CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE );
    _CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDERR );
#endif

    yyparse();

    /* yylex_destroy should be called to free resources used by the scanner */
    yylex_destroy();
}
~~~

Exécution manuelle sur un exemple :

~~~
u:\src\polytech\prairie\target\graphes>graph
Q={(1,3)}
digraph Q {
    1 -> 3;
}
$
Detected memory leaks!
Dumping objects ->
U:/src/polytech/prairie/src/yacc/graphes/graph.y(50) : {114} normal block at 0x0059B300, 13 bytes long.
 Data: <     1 -> 3; > 0A 20 20 20 20 31 20 2D 3E 20 33 3B 00
U:/src/polytech/prairie/src/yacc/graphes/graph.l(10) : {113} normal block at 0x005A1048, 2 bytes long.
 Data: <3 > 33 00
U:/src/polytech/prairie/src/yacc/graphes/graph.l(10) : {112} normal block at 0x005A1018, 2 bytes long.
 Data: <1 > 31 00
U:/src/polytech/prairie/src/yacc/graphes/graph.l(12) : {111} normal block at 0x005A0FE8, 2 bytes long.
 Data: <Q > 51 00
U:/src/polytech/prairie/target/graphes/graph_lexer.c(1733) : {110} normal block at 0x0059CFB8, 16386 bytes long.
 Data: <$   1,3)}       > 24 00 00 00 31 2C 33 29 7D 0A 00 00 CD CD CD CD
U:/src/polytech/prairie/target/graphes/graph_lexer.c(1733) : {109} normal block at 0x0059CF58, 48 bytes long.
 Data: <      Y   Y  @  > 18 DD 12 00 B8 CF 59 00 B8 CF 59 00 00 40 00 00
U:/src/polytech/prairie/target/graphes/graph_lexer.c(1733) : {108} normal block at 0x0059CF28, 4 bytes long.
 Data: <X Y > 58 CF 59 00
Object dump complete.
~~~

A ma grande surprise, je découvris une fuite de mémoire due au fait que je ne
libérais pas la mémoire allouée par `strdup()` dans la partie `Lex`. Une fois
ce problème résolu, il me restait à trouver l'objet des trois dernières
lignes du rapport de mémoire (lignes 17 à 22).

Pour cela, il faut lire la section 21.1 de la documentation de `Flex`[^7] qui
explique pourquoi cette mémoire est allouée et comment on la libère en appelant
`yylex_destroy()`. Ce petit détail n'est pas présent dans la plupart des
exemples que l'on trouve dans les ouvrages ou sur Internet, sans doute sont-ils
trop anciens[^8].

[^7]: The Default Memory Management [[http://bit.ly/UFpCns](http://flex.sourceforge.net/manual/The-Default-Memory-Management.html)]
[^8]: Memory leak - 16386 bytes allocated by malloc. [[http://bit.ly/UFpNiz](http://flex.sourceforge.net/manual/Memory-leak-_002d-16386-bytes-allocated-by-malloc_002e.html)]


Perspectives d'évolution
------------------------

J'ai développé le parcours de graphe en profondeur d'abord en Java. Il serait
donc intéressant de réécrire le parser en Java à l'aide de
[BYACC/J](http://byaccj.sourceforge.net/), ou de
[JAVACC](https://javacc.java.net/) comme le propose le projet
[JPGD](http://www.alexander-merz.com/graphviz/). Un service web accepterait
ensuite différents formats en entrée, comme le fait
[GRPH](http://www.i3s.unice.fr/~hogie/grph/) avec son système de driver.

\begin{wrapfigure}[0]{r}{0.4\textwidth}
  \vspace{-140pt}
  \begin{center}
      \includegraphics[width=6.5cm]{images/colorgraph.png}
  \end{center}
\end{wrapfigure}

\footnotesize

\ttfamily\footnotesize
\begin{tabular}{ p{0.6\textwidth} }

\begin{verbatim}
digraph G {
    1 -> 3 [color="dimgray"];
    1 -> 4 [color="dimgray"];
    1 -> 5 [color="deeppink1"];
    3 -> 2 [color="dimgray"];
    4 -> 2 [color="chartreuse2"];
    4 -> 5 [color="dimgray"];
    4 -> 7 [color="dimgray"];
    4 -> 8 [color="deeppink1"];
    5 -> 3 [color="chartreuse2"];
    5 -> 8 [color="dimgray"];
    6 -> 2 [color="chartreuse2"];
    6 -> 3 [color="chartreuse2"];
    6 -> 5 [color="chartreuse2"];
    7 -> 3 [color="chartreuse2"];
    7 -> 6 [color="dimgray"];
    7 -> 8 [color="chartreuse2"];
    8 -> 2 [color="chartreuse2"];
}
\end{verbatim}
\end{tabular}

\normalfont

S   successors       w  start    end    parent
--  -------------   --  -----   -----   ------
1   [3, 4, 5]        3     1      16       0
2   []               0     3       4       3
3   [2]              1     2       5       1
4   [2, 5, 7, 8]     4     6      15       1
5   [3, 8]           2     7      10       4
6   [2, 3, 5]        3    12      13       7
7   [3, 6, 8]        3    11      14       4
8   [2]              1     8       9       5

\normalsize

Conclusion
----------

Ce projet m'a permis de mettre en pratique des notions générales d’analyse
syntaxique et lexicale sur un langage de taille réduite. Avec `Lex` et `Yacc`,
j'ai mis en place l'analyse d'un langage DSL (Domain-Specific Language)
dont les spécifications sont dédiées au domaine d'application précis des
graphes.

En marge de l'aspect purement fonctionnel, j'ai traité la problématique de la
génération de programmes dans un contexte multi-plateformes avec CMake.

Enfin, j'ai appliqué les bonnes pratiques de développement en pilotant le projet
par les tests et en vérifiant le bon comportement du parser en environnement de
production. Cela m'a rappelé que la gestion de la mémoire est un point délicat
en C, surtout dans l'analyse lexicale[^9]. En effet, il est facile d'allouer de
la mémoire dans `Lex` et d'oublier de la libérer dans `Yacc`, le code étant
réparti sur deux fichiers différents.

[^9]: To free() or not to free() in lex/yacc [[http://bit.ly/UG1Pnr](http://bytes.com/topic/c/answers/632968-free-not-free-lex-yacc)]

