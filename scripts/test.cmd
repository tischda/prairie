:: ----------------------------------------------------------------------------
:: Prairie test script (compile and test)
:: ----------------------------------------------------------------------------
@echo off
setlocal

if "%~1"=="" goto usage

set PROJECT=%1
call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x86
set CTEST_OUTPUT_ON_FAILURE=TRUE

set SCRIPT_LOC=%~dp0
pushd %SCRIPT_LOC%\..

:: create directories
if not exist target md target
cd target
if not exist %PROJECT% md %PROJECT%
cd %PROJECT%

:: configure Makefile
cmake -G "NMake Makefiles" ..\..\src\yacc\%PROJECT% -DCMAKE_BUILD_TYPE=Debug

:: run make
nmake /nologo

:: run tests
copy ..\..\src\test\%PROJECT%\*.txt > nul
copy ..\..\src\test\%PROJECT%\*.dot > nul
copy ..\..\src\test\%PROJECT%\test.cmd > nul
nmake test

popd

goto end
::----------------------------------------------------------------------------
:usage REM Usage
echo Usage: %0 ^<project^>

:end
endlocal
