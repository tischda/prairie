#!/bin/bash
# ----------------------------------------------------------------------------
# Prairie test script (compile and test)
# ----------------------------------------------------------------------------
if [ $# -eq 0 ] ; then
    echo Usage: $0 \<project\>
    echo Projects:
    find src/yacc/* -maxdepth 1 -type d | sed 's:^src/yacc/:  :'
    exit 1
fi

PROJECT="$1"

export CTEST_OUTPUT_ON_FAILURE=TRUE

SCRIPT_LOC=$( cd "$( dirname "$0" )" && pwd )
pushd ${SCRIPT_LOC}/..

# create directory
mkdir -p target/${PROJECT}
cd target/${PROJECT}

# configure Makefile
cmake -G "Unix Makefiles" ../../src/yacc/${PROJECT} -DCMAKE_BUILD_TYPE=Debug

# run make
make

# run tests
cp ../../src/test/${PROJECT}/*.txt .
cp ../../src/test/${PROJECT}/*.dot .
cp ../../src/test/${PROJECT}/test.sh .
make test

popd
