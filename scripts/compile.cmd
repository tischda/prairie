:: ----------------------------------------------------------------------------
:: Prairie compilation script (full compile, cleans first)
::
:: Requires:
:: * Visual Studio 2013
:: * CMake 3.0
:: * Flex (cygwin)
:: * Bison (cygwin)
:: ----------------------------------------------------------------------------
@echo off
setlocal

if "%~1"=="" goto usage

set PROJECT=%1
call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x86
set SCRIPT_LOC=%~dp0
pushd %SCRIPT_LOC%\..

:: create diretories
if not exist target md target
cd target
rd /s /q %PROJECT%
md %PROJECT%
cd %PROJECT%

:: configure Makefile
cmake -G "NMake Makefiles" ..\..\src\yacc\%PROJECT% -DCMAKE_BUILD_TYPE=Debug

:: run make
nmake /nologo

popd

goto end
::----------------------------------------------------------------------------
:usage REM Usage
echo Usage: %0 ^<project^>

:end
endlocal
