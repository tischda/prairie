#!/bin/bash
# ----------------------------------------------------------------------------
# Prairie compilation script (full compile, cleans first)
# ----------------------------------------------------------------------------
if [ $# -eq 0 ] ; then
    echo Usage: $0 \<project\>
    echo Projects:
    find src/yacc/* -maxdepth 1 -type d | sed 's:^src/yacc/:  :'
    exit 1
fi

PROJECT="$1"

SCRIPT_LOC=$( cd "$( dirname "$0" )" && pwd )
pushd ${SCRIPT_LOC}/..

# create directory
rm -rf target/${PROJECT}
mkdir -p target/${PROJECT}
cd target/${PROJECT}

# configure Makefile
cmake -G "Unix Makefiles" ../../src/yacc/${PROJECT} -DCMAKE_BUILD_TYPE=Release

# run make
make

popd
