Prairie - Compilers
===================

Polytech Tours, 4ème année, 2013-1024

Description
-----------

The purpose of this project is to use Lex and Yacc to process a simple graph
DSL, for instance compiling `G = {(1,2)}` into:

~~~
digraph G {
    1 -> 2;
}
~~~


Setup
-----
On MacOS, you need XCode and the command line utilities. Install the rest via
MacPorts : `sudo port install flex bison cmake`

It's necessary to reinstall the versions from MacPorts, otherwise the libraries
needed for linking will not be found by CMake.

On Windows you need Visual Studio, CMake and Cygwin for Flex and Bison.


Compilation
-----------

Compile Lex and Yacc source code to binary with:

~~~
./scripts/compile.sh graphes
~~~

This will generate a parser into `./target/graphes`.


Testing
-------

Command:

~~~
./scripts/test.sh graphes
~~~

Output:

~~~
~/Documents/src/polytech/prairie ~/Documents/src/polytech/prairie
-- Configuring done
-- Generating done
-- Build files have been written to: /Users/daniel/Documents/src/polytech/prairie/target/graphes
[ 25%] Building C object CMakeFiles/graph.dir/graph_parser.c.o
[ 50%] Building C object CMakeFiles/graph.dir/graph_lexer.c.o
Linking C executable graph
[100%] Built target graph
Running tests...
Test project /Users/daniel/Documents/src/polytech/prairie/target/graphes
    Start 1: test1
1/3 Test #1: test1 ............................   Passed    0.01 sec
    Start 2: test2
2/3 Test #2: test2 ............................   Passed    0.01 sec
    Start 3: test3
3/3 Test #3: test3 ............................   Passed    0.01 sec

100% tests passed, 0 tests failed out of 3

Total Test time (real) =   0.02 sec
~/Documents/src/polytech/prairie
~~~

Documentation
-------------

Generate the full PDF report `./build/rapport-prairie.pdf` with:

~~~
./make.sh
~~~


References
----------

__Compilateurs : principes, techniques et outils - 2e édition__,
Alfred Aho, Monica Lam, Ravi Sethi, Jeffrey Ullman, Pearson, 2007
