if (WIN32)
    set ( C_FLAGS_WIN64 "/nologo /D_WINDOWS /D_CRT_SECURE_NO_WARNINGS /W3 /GS /D_MBCS /D_REENTRANT /wd4996 /wd4018" )
    set ( CMAKE_C_FLAGS_DEBUG   "${C_FLAGS_WIN64} /D_DEBUG /Od /Z7 /RTC1 /MTd" )
    set ( CMAKE_C_FLAGS_RELEASE "${C_FLAGS_WIN64} /DNDEBUG /O2 /Ob2 /MT" )

    # provide unistd.h from GnuWin32 project
    include_directories( "${CMAKE_CURRENT_SOURCE_DIR}/../../../include/" )

    # libraries are copied from: ...\cygwin\lib\
    set ( FL_LIBRARY "${CMAKE_CURRENT_SOURCE_DIR}/../../../lib/libfl.a" )
endif (WIN32)
